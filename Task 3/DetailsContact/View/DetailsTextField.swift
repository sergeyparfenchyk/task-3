//
//  DetailsTextField.swift
//  Task 3
//
//  Created by Sergey Parfentchik on 20.02.22.
//

import UIKit

class DetailsTextField: UITextField {

    // MARK: - Initialize

    init() {
        super.init(frame: .zero)
        initialize()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    func initialize() {
        sizeToFit()
        layer.backgroundColor = UIColor.white.cgColor
        layer.shadowRadius = 2
        layer.shadowOffset = CGSize(width: 0, height: 0)
        layer.shadowOpacity = 1
        layer.shadowColor = UIColor.gray.cgColor
        layer.cornerRadius = 6
        isEnabled = false
        translatesAutoresizingMaskIntoConstraints = false
    }

}
