//
//  DetailsLabel.swift
//  Task 3
//
//  Created by Sergey Parfentchik on 20.02.22.
//

import UIKit

class DetailsLabel: UILabel {

    // MARK: - Initialize

    init() {
        super.init(frame: .zero)
        initialize()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    func initialize() {
        translatesAutoresizingMaskIntoConstraints = false
    }
}
