//
//  DetailsContactViewController.swift
//  Task 3
//
//  Created by Sergey Parfentchik on 20.02.22.
//

import UIKit

// MARK: - Constants

private struct Constant {
    static let photoSize = 80.0
    static let photoImageViewTopOffset = 5.0
    static let stackViewSpacing = 5.0
    static let stackViewTopOffset = 5.0
    static let stackViewLeadingOffset = 10.0
    static let stackViewTralingOffset = 10.0
    static let stackViewHeigh = 225.0
    static let backgroundColor = UIColor.systemBackground
    static let titleEditButton = NSLocalizedString("Edit", comment: "")
    static let titleSaveButton = NSLocalizedString("Save", comment: "")
}

class DetailsContactViewController: UIViewController {

    // MARK: - Public Properties

    var contact: Contact?
    var indexContact: Int?

    // MARK: - Private Properties

    private var isEdit = false
    private var topConstraint = NSLayoutConstraint()

    private lazy var photoImageView: PhotoContactImageView = {
        let photoImageView = PhotoContactImageView(withSize: Constant.photoSize)
        view.addSubview(photoImageView)
        return photoImageView
    }()
    private lazy var stackView: UIStackView = {
        let stackView = UIStackView()
        stackView.axis = .vertical
        stackView.spacing = Constant.stackViewSpacing
        view.addSubview(stackView)
        stackView.translatesAutoresizingMaskIntoConstraints = false
        return stackView
    }()
    private lazy var fullNameDetailsLabel: DetailsLabel = {
        let label = DetailsLabel()
        label.text = ConstantLocalizedString.fullName
        return label
    }()
    private lazy var fullNameDetailsTextField: DetailsTextField = {
        let textField = DetailsTextField()
        textField.text = contact?.fullName
        textField.backgroundColor = ConstantColor.background
        textField.delegate = self
        return textField
    }()
    private lazy var phoneNumberDetailsLabel: DetailsLabel = {
        let label = DetailsLabel()
        label.text = ConstantLocalizedString.phoneNumber
        stackView.addSubview(label)
        return label
    }()
    private lazy var phoneNumberDetailsTextField: DetailsTextField = {
        let textField = DetailsTextField()
        textField.text = contact?.phoneNumber.first
        textField.backgroundColor = ConstantColor.background
        textField.delegate = self
        return textField
    }()

    // MARK: - Lifecycle

    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = Constant.backgroundColor
        setConstraint()
        addArrangedSubviews()
        photoImageView.image = fetchPhotoContact()
        navigationItem.rightBarButtonItem = UIBarButtonItem(title: Constant.titleEditButton,
                                                            style: .plain,
                                                            target: self,
                                                            action: #selector(editContact))
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(keyboardWillShow),
                                               name: UIResponder.keyboardWillShowNotification,
                                               object: nil)
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(keyboardWillHide),
                                               name: UIResponder.keyboardWillHideNotification,
                                               object: nil)
    }

    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.removeObserver(self, name: UIResponder.keyboardWillHideNotification, object: nil)
    }

    // MARK: - Private Method

    @objc func keyboardWillShow(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as?
                               NSValue)?.cgRectValue {
            if topConstraint.constant == Constant.photoImageViewTopOffset &&
                (UIDevice.current.orientation == .landscapeLeft ||
                 UIDevice.current.orientation == .landscapeRight) {
                topConstraint.constant += view.frame.height - keyboardSize.height - Constant.stackViewHeigh
            }
        }
        view.layoutIfNeeded()
    }

    @objc func keyboardWillHide(notification: NSNotification) {
        topConstraint.constant = Constant.photoImageViewTopOffset
    }

    private func addArrangedSubviews() {
        stackView.addArrangedSubview(fullNameDetailsLabel)
        stackView.addArrangedSubview(fullNameDetailsTextField)
        stackView.addArrangedSubview(phoneNumberDetailsLabel)
        stackView.addArrangedSubview(phoneNumberDetailsTextField)
    }

    private func setConstraint() {
        topConstraint = photoImageView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor,
                                                            constant: Constant.photoImageViewTopOffset)
        NSLayoutConstraint.activate([
            topConstraint,
            photoImageView.centerXAnchor.constraint(equalTo: view.centerXAnchor),

            stackView.topAnchor.constraint(equalTo: photoImageView.bottomAnchor,
                                           constant: Constant.stackViewTopOffset),
            stackView.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor,
                                               constant: Constant.stackViewLeadingOffset),
            stackView.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor,
                                                constant: -Constant.stackViewTralingOffset)
        ])
    }

    private func fetchPhotoContact() -> UIImage? {
        weak var selfWeak = self
        let photo = UIImage(systemName: ConstantString.imageNamePersonCropCircle)
        if let path = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first {
            let diskSorage = DiskStorage(path: path)
            diskSorage.fetchValue(for: contact!.imageName) { result in
                do {
                    let data = try result.get()
                    let photo = UIImage(data: data)
                    let queue = DispatchQueue.main
                    queue.sync {
                        selfWeak?.photoImageView.image = photo
                        selfWeak?.photoImageView.setNeedsLayout()
                    }
                } catch { }
            }
        }
        return photo
    }

    @objc private func editContact(barButtonItem: UIBarButtonItem) {
        isEdit = !isEdit
        if isEdit {
            barButtonItem.title = Constant.titleSaveButton
            fullNameDetailsTextField.isEnabled = true
            phoneNumberDetailsTextField.isEnabled = true
            fullNameDetailsTextField.becomeFirstResponder()
        } else {
            barButtonItem.title = Constant.titleEditButton
            fullNameDetailsTextField.isEnabled = false
            phoneNumberDetailsTextField.isEnabled = false
            saveContact()
        }
    }

    private func saveContact() {
        if let allContactsViewController = navigationController?.viewControllers.first as? AllContactsViewController {
            if let index = indexContact {
                var tmpContact = contact
                tmpContact?.fullName = fullNameDetailsTextField.text ?? ""
                tmpContact?.phoneNumber.insert(phoneNumberDetailsTextField.text ?? "", at: 0)
                if let tmpContact = tmpContact {
                    allContactsViewController.contacts[index] = tmpContact
                }
            }
        }
    }
}

// MARK: - UITextViewDelegate

extension DetailsContactViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == fullNameDetailsTextField {
            phoneNumberDetailsTextField.becomeFirstResponder()
        } else {
            view.endEditing(true)
        }
        return false
    }
}
