//
//  FavoriteContactsViewController.swift
//  Task 3
//
//  Created by Sergey Parfentchik on 17.02.22.
//

import UIKit

// MARK: - Constants

private struct Constant {
    static let heightCell = 60.0
}

class FavoriteContactsViewController: UIViewController {

    // MARK: - Private property

    private lazy var favoriteContacts: [Contact] = []
    private lazy var contactsTableView: UITableView = {
        let tableView = UITableView()
        tableView.delegate = self
        tableView.dataSource = self
        view.addSubview(tableView)
        tableView.register(ContactTableViewCell.self,
                           forCellReuseIdentifier: ConstantString.contactTableViewCellId)
        tableView.translatesAutoresizingMaskIntoConstraints = false
        return tableView
    }()
    private lazy var checkLabel: UILabel = {
        let label = UILabel()
        label.text = ConstantLocalizedString.thereNothingHere
        view.addSubview(label)
        label.translatesAutoresizingMaskIntoConstraints = false
        label.isHidden = true
        return label
    }()

    // MARK: - Lifecycle

    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = ConstantColor.background
        setConstraint()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        favoriteContacts = []
        let viewController = tabBarController?.delegate as? AllContactsViewController
        var allContactsViewController = AllContactsViewController()
        if let viewController = viewController {
            allContactsViewController = viewController
        }
        for contact in allContactsViewController.contacts where contact.favorite {
            favoriteContacts.append(contact)
        }
        contactsTableView.reloadData()
        if favoriteContacts.count == 0 {
            checkLabel.isHidden = false
        } else {
            checkLabel.isHidden = true
        }
    }

    private func fetchPhotoContact(_ imageName: String, cellForRowAt indexPath: IndexPath) -> UIImage? {
        weak var selfWeak = self
        let photo = UIImage(systemName: ConstantString.defaultPhotoName)
        if let path = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first {
            let diskSorage = DiskStorage(path: path)
            diskSorage.fetchValue(for: imageName) { result in
                do {
                    let data = try result.get()
                    let photo = UIImage(data: data)
                    let queue = DispatchQueue.main
                    queue.sync {
                        let cell = selfWeak?.contactsTableView.cellForRow(at: indexPath) as? ContactTableViewCell
                        cell?.photoImageView.image = photo
                        cell?.setNeedsLayout()
                    }
                } catch { }
            }
        }
        return photo
    }

    private func setConstraint() {
        NSLayoutConstraint.activate([
            contactsTableView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor),
            contactsTableView.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor),
            contactsTableView.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            contactsTableView.bottomAnchor.constraint(equalTo: view.bottomAnchor),

            checkLabel.centerXAnchor.constraint(equalTo: view.centerXAnchor),
            checkLabel.centerYAnchor.constraint(equalTo: view.centerYAnchor)
        ])
    }
}

// MARK: - UITableViewDelegate

extension FavoriteContactsViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        Constant.heightCell
    }
}

// MARK: - UITableViewDataSource

extension FavoriteContactsViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        favoriteContacts.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: ConstantString.contactTableViewCellId,
                                                 for: indexPath) as? ContactTableViewCell
        if let contactCell = cell {
            contactCell.photoImageView.image = fetchPhotoContact(favoriteContacts[indexPath.row].imageName,
                                                                 cellForRowAt: indexPath)
            contactCell.fullNameLabel.text = favoriteContacts[indexPath.row].fullName
            contactCell.phoneNumberLabel.text = favoriteContacts[indexPath.row].phoneNumber.first
            return contactCell
        } else {
            let contactCell = ContactTableViewCell()
            return contactCell
        }
    }
}
