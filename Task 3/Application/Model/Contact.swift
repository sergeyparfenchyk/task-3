//
//  Contact.swift
//  Task 3
//
//  Created by Sergey Parfentchik on 17.02.22.
//

import Foundation

struct Contact: Codable {
    var fullName: String
    var phoneNumber: [String]
    let imageName: String
    var favorite: Bool
}
