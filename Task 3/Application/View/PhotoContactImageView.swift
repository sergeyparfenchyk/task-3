//
//  PhotoContactImageView.swift
//  Task 3
//
//  Created by Sergey Parfentchik on 21.02.22.
//

import UIKit

class PhotoContactImageView: UIImageView {

    // MARK: - Private Properties

    private let size: CGFloat
    private let cornerRadius: CGFloat

    // MARK: - Initialize

    init(withSize size: CGFloat) {
        self.size = size
        self.cornerRadius = size / 2
        super.init(frame: .zero)
        initialize()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    private func initialize() {
        layer.cornerRadius = cornerRadius
        layer.masksToBounds = true
        translatesAutoresizingMaskIntoConstraints = false
        settingConstraints()
    }

    // MARK: - Private Methods

    private func settingConstraints() {
        NSLayoutConstraint.activate([
            self.heightAnchor.constraint(equalToConstant: size),
            self.widthAnchor.constraint(equalToConstant: size)
        ])
    }
}
