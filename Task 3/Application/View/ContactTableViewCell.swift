//
//  ContactTableViewCell.swift
//  Task 3
//
//  Created by Sergey Parfentchik on 19.02.22.
//

import UIKit

// MARK: - Constants

private struct Constant {
    static let photoLeadingOffset = 20.0
    static let photoSize = 40.0
    static let fullNameLeadingOffset = 10.0
    static let fullNameTrailingOffset = 50.0
    static let fullNameTopOffset = 5.0
    static let phoneNumberTopOffset = 5.0
    static let phoneNumberBottomOffset = 8.0
    static let fonttTextPhoneNumberTableViewCell = 14.0
}

class ContactTableViewCell: UITableViewCell {

    // MARK: - Public Properties

    lazy var photoImageView: PhotoContactImageView = {
        let photoImageView = PhotoContactImageView(withSize: Constant.photoSize)
        addSubview(photoImageView)
        return photoImageView
    }()
    lazy var fullNameLabel: UILabel = {
        let fullNameLabel = UILabel()
        addSubview(fullNameLabel)
        fullNameLabel.textColor = ConstantColor.textFullName
        fullNameLabel.translatesAutoresizingMaskIntoConstraints = false
        return fullNameLabel
    }()
    lazy var phoneNumberLabel: UILabel = {
        let phoneNumberLabel = UILabel()
        phoneNumberLabel.textColor = ConstantColor.textPhoneNumber
        addSubview(phoneNumberLabel)
        phoneNumberLabel.translatesAutoresizingMaskIntoConstraints = false
        phoneNumberLabel.font = UIFont.systemFont(ofSize: Constant.fonttTextPhoneNumberTableViewCell)
        return phoneNumberLabel
    }()

    // MARK: - Initialize

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: .subtitle, reuseIdentifier: reuseIdentifier)
        initialize()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    private func initialize() {
        setConstraints()
    }

    // MARK: - Lifecycle

    override func prepareForReuse() {
        super.prepareForReuse()
        photoImageView.image = nil
    }

    // MARK: - Private Methods

    private func setConstraints() {

        NSLayoutConstraint.activate([
            photoImageView.leadingAnchor.constraint(equalTo: contentView.leadingAnchor,
                                                    constant: Constant.photoLeadingOffset),
            photoImageView.centerYAnchor.constraint(equalTo: contentView.centerYAnchor),

            fullNameLabel.leadingAnchor.constraint(equalTo: photoImageView.trailingAnchor,
                                                   constant: Constant.fullNameLeadingOffset),
            fullNameLabel.trailingAnchor.constraint(lessThanOrEqualTo: self.trailingAnchor,
                                                    constant: -Constant.fullNameTrailingOffset),
            fullNameLabel.topAnchor.constraint(equalTo: contentView.topAnchor,
                                               constant: Constant.fullNameTopOffset),

            phoneNumberLabel.leadingAnchor.constraint(equalTo: fullNameLabel.leadingAnchor),
            phoneNumberLabel.trailingAnchor.constraint(equalTo: fullNameLabel.trailingAnchor),
            phoneNumberLabel.topAnchor.constraint(greaterThanOrEqualTo: fullNameLabel.bottomAnchor,
                                                  constant: Constant.phoneNumberTopOffset),
            phoneNumberLabel.bottomAnchor.constraint(equalTo: contentView.bottomAnchor,
                                                     constant: -Constant.phoneNumberBottomOffset)
        ])
    }
}
