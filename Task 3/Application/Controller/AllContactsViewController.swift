//
//  AllContactsViewController.swift
//  Task 3
//
//  Created by Sergey Parfentchik on 17.02.22.
//

import UIKit
import Contacts

// MARK: - Constants

private struct Constant {
    static let defaultPath = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first
    static let heightCell = 60.0
}

class AllContactsViewController: UIViewController, UITabBarControllerDelegate {

    // MARK: - Public property

    var contacts: [Contact] = []

    // MARK: - Private property

    private lazy var contactsTableView: UITableView = {
        let tableView = UITableView()
        tableView.delegate = self
        tableView.dataSource = self
        view.addSubview(tableView)
        tableView.register(ContactTableViewCell.self,
                           forCellReuseIdentifier: ConstantString.contactTableViewCellId)
        tableView.translatesAutoresizingMaskIntoConstraints = false
        return tableView
    }()

    // MARK: - Lifecycle

    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = ConstantColor.background
        title = ConstantLocalizedString.contacts
        tabBarController?.delegate = self
        setConstraint()
        fetchContacts()
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if checkContactsIsEmpty() {
            presentAddingContactsViewController()
        } else {
            saveContacts()
            contactsTableView.reloadData()
        }
    }

    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        saveContacts()
    }

    // MARK: - Private Metchod

    private func setConstraint() {
        NSLayoutConstraint.activate([
            contactsTableView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor),
            contactsTableView.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor),
            contactsTableView.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            contactsTableView.bottomAnchor.constraint(equalTo: view.bottomAnchor)
        ])
    }

    private func importContactsFromContactStore() {
        let contactStore = CNContactStore()
        let keysToFetch = [CNContactIdentifierKey as CNKeyDescriptor,
                           CNContactImageDataKey as CNKeyDescriptor,
                           CNContactPhoneNumbersKey as CNKeyDescriptor,
                           CNContactFormatter.descriptorForRequiredKeys(for: .fullName)]
        let contactFetchRequest = CNContactFetchRequest(keysToFetch: keysToFetch)
        do {
            try contactStore.enumerateContacts(with: contactFetchRequest) { contact, _ in
                var tmpContact = Contact(fullName: CNContactFormatter.string(from: contact, style: .fullName) ?? "",
                                         phoneNumber: [],
                                         imageName: contact.identifier,
                                         favorite: false)
                for phoneNumber in contact.phoneNumbers {
                    tmpContact.phoneNumber.append(phoneNumber.value.stringValue)
                }
                self.contacts.append(tmpContact)
                if let data = contact.imageData {
                    if let path = Constant.defaultPath {
                        let diskSorage = DiskStorage(path: path)
                        diskSorage.save(value: data, for: tmpContact.imageName) { _ in }
                    }
                }
            }
        } catch {
        }
    }

    private func fetchContacts() {
        if let path = Constant.defaultPath {
            let diskSorage = DiskStorage(path: path)
            let codableStorage = CodableStorage(storage: diskSorage)
            do {
                try contacts = codableStorage.fetch(for: ConstantString.nameFileContacts)
            } catch {
                contacts = []
            }
        }
    }

    private func saveContacts() {
        let queue = DispatchQueue.global(qos: .userInteractive)
        queue.async {
            if let path = Constant.defaultPath {
                let diskSorage = DiskStorage(path: path)
                let codableStorage = CodableStorage(storage: diskSorage)
                do {
                    try codableStorage.save(self.contacts, for: ConstantString.nameFileContacts)
                } catch {
                }
            }
        }
    }

    private func fetchPhotoContact(_ imageName: String, cellForRowAt indexPath: IndexPath) -> UIImage? {
        weak var selfWeak = self
        let photo = UIImage(systemName: ConstantString.defaultPhotoName)
        if let path = Constant.defaultPath {
            let diskSorage = DiskStorage(path: path)
            diskSorage.fetchValue(for: imageName) { result in
                do {
                    let data = try result.get()
                    let photo = UIImage(data: data)
                    let queue = DispatchQueue.main
                    queue.sync {
                        let cell = selfWeak?.contactsTableView.cellForRow(at: indexPath) as? ContactTableViewCell
                        cell?.photoImageView.image = photo
                        cell?.setNeedsLayout()
                    }
                } catch { }
            }
        }
        return photo
    }

    private func createContactViewControler(indexRow: Int) -> UIViewController {
        let contactViewController = DetailsContactViewController()
        contactViewController.contact = contacts[indexRow]
        contactViewController.indexContact = indexRow
        return contactViewController
    }

    private func getContactCell(_ tableView: UITableView,
                                ForRowAt indexPath: IndexPath) -> ContactTableViewCell {
        var contactCell = ContactTableViewCell()
        let cell = tableView.cellForRow(at: indexPath) as? ContactTableViewCell
        if let cell = cell {
            contactCell = cell
        }
        return contactCell
    }

    private func createAlertController(_ tableView: UITableView, ForRowAt indexPath: IndexPath) {
        let cellContact = getContactCell(tableView, ForRowAt: indexPath)

        let alert = UIAlertController(title: cellContact.fullNameLabel.text, message: nil, preferredStyle: .actionSheet)
        alert.addAction(UIAlertAction(title: ConstantLocalizedString.copyPhoneNumber,
                                      style: .default,
                                      handler: { (_) in
            UIPasteboard.general.string = cellContact.phoneNumberLabel.text
        }))

        alert.addAction(UIAlertAction(title: ConstantLocalizedString.sharePhoneNumber,
                                      style: .default,
                                      handler: { (_) in
            let phoneNumber = cellContact.phoneNumberLabel.text
            let actionViewController = UIActivityViewController(activityItems: [phoneNumber as Any],
                                                                applicationActivities: nil)
            self.present(actionViewController, animated: true, completion: nil)
        }))

        alert.addAction(UIAlertAction(title: ConstantLocalizedString.deleteContact,
                                      style: .default,
                                      handler: { (_) in
            self.deleteContact(tableView, ForRowAt: indexPath)
        }))

        alert.addAction(UIAlertAction(title: ConstantLocalizedString.cancel, style: .cancel, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }

    private func deleteContact(_ tableView: UITableView, ForRowAt indexPath: IndexPath) {
        contacts.remove(at: indexPath.row)
        tableView.deleteRows(at: [indexPath], with: .top)
        if checkContactsIsEmpty() {
            presentAddingContactsViewController()
        }
    }

    private func checkContactsIsEmpty() -> Bool {
        return contacts.count == 0 ? true : false
    }

    private func presentAddingContactsViewController() {
        let importContactsViewController = ImportContactsViewController()
        importContactsViewController.delegate = self
        importContactsViewController.modalPresentationStyle = .fullScreen
        self.present(importContactsViewController, animated: true, completion: nil)
    }

    private func createFavoriteButton(fill: Bool, withTag tag: Int) -> UIButton {
        let favoriteButton = UIButton(type: .roundedRect)
        favoriteButton.tag = tag
        favoriteButton.sizeToFit()
        favoriteButton.addTarget(self, action: #selector(changeFavoritButton), for: .touchUpInside)
        if fill {
            favoriteButton.setImage(UIImage(systemName: ConstantString.imageNameHeartFill), for: .normal)
        } else {
            favoriteButton.setImage(UIImage(systemName: ConstantString.imageNameHeart), for: .normal)
        }
        return favoriteButton
    }

    @objc private func changeFavoritButton(button: UIButton) {
        contacts[button.tag].favorite = !contacts[button.tag].favorite
        let trueImage = UIImage(systemName: ConstantString.imageNameHeartFill)
        let falseImage = UIImage(systemName: ConstantString.imageNameHeart)
        let imageButton = contacts[button.tag].favorite ? trueImage : falseImage
        button.setImage(imageButton, for: .normal)
    }
}

// MARK: - ImportContactsViewControllerDelegate

extension AllContactsViewController: ImportContactsViewControllerDelegate {
    func importContactsViewControllerGetContacts(_ importContactsViewController: ImportContactsViewController) {
        if contacts.count > 0 { return }
        importContactsFromContactStore()
        contactsTableView.reloadData()
        saveContacts()
    }
}

// MARK: - UITableViewDelegate

extension AllContactsViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        Constant.heightCell
    }

    func tableView(_ tableView: UITableView,
                   trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        let delete = UIContextualAction(style: .destructive, title: "Delete") { _, _, complete in
            self.deleteContact(tableView, ForRowAt: indexPath)
            complete(true)
        }
        let configuration = UISwipeActionsConfiguration(actions: [delete])
        configuration.performsFirstActionWithFullSwipe = true
        return configuration
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let contactViewController = createContactViewControler(indexRow: indexPath.row)
        self.navigationController?.pushViewController(contactViewController, animated: true)
    }

    func tableView(_ tableView: UITableView,
                   contextMenuConfigurationForRowAt indexPath: IndexPath,
                   point: CGPoint) -> UIContextMenuConfiguration? {
        createAlertController(tableView, ForRowAt: indexPath)
        let menu = UIContextMenuConfiguration()
        return menu
    }
}

// MARK: - UITableViewDataSource

extension AllContactsViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        contacts.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: ConstantString.contactTableViewCellId,
                                                 for: indexPath) as? ContactTableViewCell
        if let contactCell = cell {
            contactCell.photoImageView.image = fetchPhotoContact(contacts[indexPath.row].imageName,
                                                                 cellForRowAt: indexPath)
            contactCell.fullNameLabel.text = contacts[indexPath.row].fullName
            contactCell.phoneNumberLabel.text = contacts[indexPath.row].phoneNumber.first
            contactCell.accessoryType = .checkmark
            let favoriteButton = createFavoriteButton(fill: contacts[indexPath.row].favorite,
                                                      withTag: indexPath.row)
            contactCell.accessoryView = favoriteButton
            contactCell.accessoryType = .detailButton
            return contactCell
        } else {
            let contactCell = ContactTableViewCell()
            return contactCell
        }
    }
}
