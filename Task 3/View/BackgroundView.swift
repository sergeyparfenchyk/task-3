//
//  ImageBackgroundView.swift
//  Task 3
//
//  Created by Sergey Parfentchik on 19.02.22.
//

import UIKit

class ImageBackgroundView: UIView {

    // MARK: - Public Properties

    lazy var image = UIImageView()

    // MARK: - Private Properties

    private let size: CGFloat
    private let borderWidth: CGFloat
    private let cornerRadius: CGFloat

    // MARK: - Initialize

    convenience init() {
        self.init(imageZoom: 1.0)
    }

    init(imageZoom zoom: CGFloat) {
        self.size = Const.Image.size * zoom
        self.borderWidth = self.size / 8.0
        self.cornerRadius = self.size / 2
        super.init(frame: .zero)
        initialize()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Private Methods

    private func initialize() {
        backgroundColor = Const.Image.backgroundColor
        layer.cornerRadius = cornerRadius
        settingImageView()
        addingSubviews()
        notTranslatesAutoresizingMaskIntoConstraints()
        settingConstraints()
    }

    private func settingImageView() {
        image.contentMode = .scaleAspectFit
        image.alpha = Const.Image.alpha
    }

    private func addingSubviews() {
        self.addSubview(image)
    }

    private func notTranslatesAutoresizingMaskIntoConstraints() {
        self.translatesAutoresizingMaskIntoConstraints = false
        image.translatesAutoresizingMaskIntoConstraints = false
    }

    private func settingConstraints() {
        NSLayoutConstraint.activate([
            self.heightAnchor.constraint(equalToConstant: size),
            self.widthAnchor.constraint(equalToConstant: size),

            image.topAnchor.constraint(equalTo: self.topAnchor, constant: borderWidth),
            image.bottomAnchor.constraint(equalTo: self.bottomAnchor, constant: -borderWidth),
            image.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: borderWidth),
            image.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: -borderWidth)
        ])
    }
}
