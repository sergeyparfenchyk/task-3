//
//  ImportContactsViewController.swift
//  Task 3
//
//  Created by Sergey Parfentchik on 17.02.22.
//

import UIKit
import Contacts

// MARK: - Constants

private struct Constant {
    static let backgroundColorDownloadContactsButton = UIColor.systemBlue
    static let cornerRadiusDownloadContactsButton = 10.0
    static let textColorDownloadContactsButton = UIColor.white
    static let widthDownloadContactsButton = 200.0
}

class ImportContactsViewController: UIViewController {

    // MARK: - Public Property

    weak var delegate: ImportContactsViewControllerDelegate?

    // MARK: - Private Property

    private let allContactsViewController = AllContactsViewController()
    private lazy var downloadContactsButton: UIButton = {
        let button = UIButton(type: .system)
        button.setTitle(ConstantLocalizedString.downloadContacts, for: .normal)
        button.setTitleColor(Constant.textColorDownloadContactsButton,
                             for: .normal)
        button.addTarget(self, action: #selector(touchUpInsideDownloadContactsButton),
                         for: .touchUpInside)
        button.layer.cornerRadius = Constant.cornerRadiusDownloadContactsButton
        view.addSubview(button)
        button.backgroundColor = Constant.backgroundColorDownloadContactsButton
        button.translatesAutoresizingMaskIntoConstraints = false
        return button
    }()

    // MARK: - Lifecycle

    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = ConstantColor.background
        setConstrants()
    }

    // MARK: - Private Methods

    private func setConstrants() {
        NSLayoutConstraint.activate([
            downloadContactsButton.centerXAnchor.constraint(equalTo: view.centerXAnchor),
            downloadContactsButton.centerYAnchor.constraint(equalTo: view.centerYAnchor),
            downloadContactsButton.widthAnchor.constraint(equalToConstant: Constant.widthDownloadContactsButton)
        ])
    }

    @objc private func touchUpInsideDownloadContactsButton() {
        let authorizationStatus = CNContactStore.authorizationStatus(for: .contacts)
        guard authorizationStatus != .authorized else {
            delegate?.importContactsViewControllerGetContacts(self)
            self.dismiss(animated: true, completion: nil)
            return
        }
        guard authorizationStatus != .notDetermined else {
            delegate?.importContactsViewControllerGetContacts(self)
            if authorizationStatus == .notDetermined {
                touchUpInsideDownloadContactsButton()
            } else {
                self.dismiss(animated: true, completion: nil)
            }
            return
        }
        guard authorizationStatus != .denied else {
            showErrorAlertController()
            return
        }
    }

    private func showErrorAlertController() {
        let errorAlertController = UIAlertController(title: ConstantLocalizedString.error,
                                                     message: ConstantLocalizedString.youAccessToContacts,
                                                     preferredStyle: .alert)
        let okButton = UIAlertAction(title: ConstantLocalizedString.okey, style: .default, handler: { (_) -> Void in
            if let bundleIdentifier = Bundle.main.bundleIdentifier,
               let appSettings = URL(string: UIApplication.openSettingsURLString + bundleIdentifier) {
                if UIApplication.shared.canOpenURL(appSettings) {
                    UIApplication.shared.open(appSettings)
                }
            }
        })

        errorAlertController.addAction(okButton)
        self.present(errorAlertController, animated: true, completion: nil)
    }
}
