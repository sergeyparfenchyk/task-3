//
//  ImportContactsViewControllerDelegate.swift
//  Task 3
//
//  Created by Sergey Parfentchik on 19.02.22.
//

import Foundation

protocol ImportContactsViewControllerDelegate: AnyObject {
    func importContactsViewControllerGetContacts(_ importContactsViewController: ImportContactsViewController)
}
