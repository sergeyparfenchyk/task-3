//
//  SceneDelegate.swift
//  Task 3
//
//  Created by Sergey Parfentchik on 17.02.22.
//

import UIKit

class SceneDelegate: UIResponder, UIWindowSceneDelegate {

    var window: UIWindow?

    func scene(_ scene: UIScene,
               willConnectTo session: UISceneSession,
               options connectionOptions: UIScene.ConnectionOptions) {
        guard let windowScene = (scene as? UIWindowScene) else { return }
        window = UIWindow(frame: windowScene.coordinateSpace.bounds)
        window?.windowScene = windowScene
        window?.rootViewController = createTabBarViewController()
        window?.makeKeyAndVisible()
    }

    private func createTabBarViewController() -> UIViewController {
        let contactTabBarbViewController = UITabBarController()
        let allContactsViewController = createAllContactsViewController()
        let favoriteContactsViewController = FavoriteContactsViewController()
        let allContactsTabBarItem = UITabBarItem(title: ConstantLocalizedString.contacts,
                                                 image: UIImage(systemName: ConstantString.imageNamePerson2),
                                                 tag: 0)
        allContactsTabBarItem.selectedImage = UIImage(systemName: ConstantString.imageNamePerson2Fill)
        allContactsViewController.tabBarItem = allContactsTabBarItem
        let bestContactTabBarItem = UITabBarItem(title: ConstantLocalizedString.favorites,
                                                 image: UIImage(systemName: ConstantString.imageNameHeart),
                                                 tag: 1)
        bestContactTabBarItem.selectedImage = UIImage(systemName: ConstantString.imageNameHeartFill)
        favoriteContactsViewController.tabBarItem = bestContactTabBarItem
        contactTabBarbViewController.viewControllers = [allContactsViewController,
                                                        favoriteContactsViewController]
        return contactTabBarbViewController
    }

    private func createAllContactsViewController() -> UIViewController {
        let allContactsViewController = AllContactsViewController()
        let allContactsNavigationController = UINavigationController(rootViewController: allContactsViewController)
        return allContactsNavigationController
    }
}
