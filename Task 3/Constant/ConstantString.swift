//
//  ConstantString.swift
//  Task 3
//
//  Created by Sergey Parfentchik on 19.02.22.
//

import Foundation

struct ConstantString {
    static let contactTableViewCellId = "CellId"
    static let detailsTableViewCellId = "DetailsCellId"
    static let imageNamePerson2 = "person.2"
    static let imageNamePerson2Fill = "person.2.fill"
    static let imageNameHeart = "heart"
    static let imageNameHeartFill = "heart.fill"
    static let nameFileContacts = "Contacts"
    static let defaultPhotoName = "person.crop.circle"
    static let imageNamePersonCropCircle = "person.crop.circle"
}
