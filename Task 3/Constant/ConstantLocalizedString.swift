//
//  ConstantLocalizedString.swift
//  Task 3
//
//  Created by Sergey Parfentchik on 21.02.22.
//

import Foundation

struct ConstantLocalizedString {
    static let downloadContacts = NSLocalizedString("Download contacts", comment: "")
    static let contacts = NSLocalizedString("Contacts", comment: "")
    static let favorites = NSLocalizedString("Favorites", comment: "")
    static let fullName = NSLocalizedString("Full Name", comment: "")
    static let phoneNumber = NSLocalizedString("Phone Number", comment: "")
    static let copyPhoneNumber = NSLocalizedString("Copy phone number", comment: "")
    static let sharePhoneNumber = NSLocalizedString("Share phone number", comment: "")
    static let deleteContact = NSLocalizedString("Delete contact", comment: "")
    static let cancel = NSLocalizedString("Cancel", comment: "")
    static let okey = NSLocalizedString("Ok", comment: "")
    static let error = NSLocalizedString("Error", comment: "")
    static let youAccessToContacts = NSLocalizedString("You must provide access to Contacts.", comment: "")
    static let thereNothingHere = NSLocalizedString("There's nothing here", comment: "")
}
