//
//  ConstantColor.swift
//  Task 3
//
//  Created by Sergey Parfentchik on 19.02.22.
//

import UIKit

struct ConstantColor {
    static let textFullName = UIColor.label
    static let textPhoneNumber = UIColor.systemGray2
    static let background = UIColor.systemBackground
}
